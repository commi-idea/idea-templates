
# idea-templates

## How To Use

- Go to IDEA;
- Ctrl+Alt+S to open Settings;
- Go to Tools / Settings Repository;
- Add current repository to read-only sources;
- Enjoy (this is mandatory);


- Docs: https://www.jetbrains.com/help/idea/settings-tools-settings-repository.html

## Live Templates

- Asterisk (*) marks cursor location when inserting template.

### Java Core

#### notimpl

```java
throw new NotImplementedException*();
```

#### of

```java
public static T of() {
	*
}
```

#### psvm

```java
public static void main(String... args) {
	*
}
```

#### result

```java
T result;
*
return result;
```

#### result=

```java
T result = *;
return result;
```

#### reqnn

```java
Objects.requireNonNull(argument,"argument");*
```

#### unsupp

```java
throw new UnsupportedOperationException*();
```

### Java EE

#### postconstruct

```java
@PostConstruct
private void postConstruct() {
	*
}
```

#### predestroy

```java
@PreDestroy
private void preDestroy() {
	*
}
```

### Java Lombok

#### abvw

```java
@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
```

#### builder

```java
@Builder(builderClassName = "Builder", toBuilder = true)
```

#### bvw

```java
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
```

#### valueof

```java
@Value(staticConstructor = "of")
```

### Javadoc

#### anchor

```java
<a href="LINK">LINK</a>
```

### JUnit 5

#### beforeall

```java
@BeforeAll
private void beforeAll() {
	* 
}
```

#### beforeeach

```java
@BeforeEach
private void beforeEach() {
	*
}
```

### Quarkus

#### onstartup

```java
private void onStartup(@Observes StartupEvent ignored) {
	*
}
```

## File Templates

### Known Issues

- As of 2020.3.2 fileTemplates are not syncing.
  To make it work copy <b>fileTemplates</b> folder to the root of your IDEA configuration.
  - This is {user}/AppData/Roaming/JetBrains/{idea} by default for Windows.